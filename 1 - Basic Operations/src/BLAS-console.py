#!/usr/bin/python

import numpy as np
import csv

with open('../files/M.csv') as M_csv:
  M_reader = list(csv.reader(M_csv, delimiter=';'))
with open('../files/N.csv') as N_csv:
  N_reader = list(csv.reader(N_csv, delimiter=';'))
with open('../files/a.csv') as a_csv:
  a_reader = list(csv.reader(a_csv, delimiter=';'))

M = np.mat(M_reader[0:], dtype=np.int)
N = np.mat(N_reader[0:], dtype=np.int)
a = np.array(a_reader[0:], dtype=np.float)

try: 
  MN = M @ N
  aM = (a @ M).round(2)
  Ma = (M @ a).round(2)
except ValueError: 
  print("Something went wrong with M * a operation")
try:
  print('M * N: \n', MN)
  print('a * M: \n', aM)
  print('M * a: \n', Ma)
except NameError:
  print("Variable Ma is not defined")